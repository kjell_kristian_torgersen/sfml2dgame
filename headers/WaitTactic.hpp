/*
 * WaitTactic.hpp
 *
 *  Created on: Apr 19, 2017
 *      Author: kjell
 */

#ifndef WAITTACTIC_HPP_
#define WAITTACTIC_HPP_
#include "Game.hpp"
#include "Enemy.hpp"
#include "ITactic.hpp"

class WaitTactic: public ITactic {
public:
	WaitTactic(Enemy * enemy);
	bool Iter(float dt);
	virtual ~WaitTactic();
private:
	Enemy * enemy;
};

#endif /* WAITTACTIC_HPP_ */
