/*
 * global.hpp
 *
 *  Created on: Apr 18, 2017
 *      Author: kjell
 */

#ifndef GLOBAL_HPP_
#define GLOBAL_HPP_
#include <SFML/System.hpp>

float dist(sf::Vector2f v);
float time_of_impact(sf::Vector2f p, sf::Vector2f v, float s);
typedef unsigned int uint;
typedef sf::Vector2<double> Vector2d;

extern class Game * game;

#endif /* GLOBAL_HPP_ */
