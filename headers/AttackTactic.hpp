/*
 * AttackTactic.hpp
 *
 *  Created on: Apr 21, 2017
 *      Author: kjell
 */

#ifndef ATTACKTACTIC_HPP_
#define ATTACKTACTIC_HPP_

#include "ITactic.hpp"
#include "Enemy.hpp"

class AttackTactic: public ITactic {
public:
	AttackTactic(Enemy * enemy);
	bool Iter(float dt);
	virtual ~AttackTactic();
private:
	Enemy * enemy;
	float timer;
	float speed;
	float range;
};

#endif /* ATTACKTACTIC_HPP_ */
