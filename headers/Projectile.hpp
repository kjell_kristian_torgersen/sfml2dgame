/*
 * Projectile.hpp
 *
 *  Created on: Apr 18, 2017
 *      Author: kjell
 */

#ifndef PROJECTILE_HPP_
#define PROJECTILE_HPP_

class Game;

#include "IEntity.hpp"
#include <SFML/Graphics.hpp>


class Projectile: public IEntity {
public:
	Projectile(void);
	Projectile(sf::Vector2f pos, sf::Vector2f vel, sf::Sprite sprite, IEntity * attacker, float damage);
	Kinematic & Kin();
	sf::Sprite * Sprite();
	void Damage(float damage);
	bool Iter(float dt);
	float Angle(void);
	bool IsDead(void);
	virtual ~Projectile();
private:
	Kinematic kin;
	sf::Sprite sprite;
	IEntity * attacker;
	float damage;
};

#endif /* PROJECTILE_HPP_ */
