/*
 * Weapon.hpp
 *
 *  Created on: Apr 20, 2017
 *      Author: kjell
 */

#ifndef WEAPON_HPP_
#define WEAPON_HPP_
#include <SFML/System.hpp>
#include "IWeapon.hpp"
#include "Enemy.hpp"

class Weapon: public IWeapon {
public:
	Weapon();
	bool Fire(sf::Vector2f target);
	bool Fire(IEntity * target);
	void SetOwner(IEntity * owner);
	virtual ~Weapon();
private:
	IEntity * owner;
};

#endif /* WEAPON_HPP_ */
