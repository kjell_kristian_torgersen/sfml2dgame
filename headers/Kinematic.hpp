/*
 * Kinematic.hpp
 *
 *  Created on: Apr 20, 2017
 *      Author: kjell
 */

#ifndef KINEMATIC_HPP_
#define KINEMATIC_HPP_
#include  "global.hpp"

class Kinematic {
public:
	sf::Vector2f Pos;
	sf::Vector2f Vel;
	Kinematic(void);
	Kinematic(sf::Vector2f Pos, sf::Vector2f Vel);
	void Iter(float dt);
	virtual ~Kinematic();
};

#endif /* KINEMATIC_HPP_ */
