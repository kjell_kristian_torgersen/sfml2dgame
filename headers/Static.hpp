/*
 * Static.hpp
 *
 *  Created on: Apr 24, 2017
 *      Author: kjell
 */

#ifndef STATIC_HPP_
#define STATIC_HPP_

#include "IEntity.hpp"

class Game;

class Static: public IEntity {
public:
	Static(sf::Vector2f pos, sf::Sprite sprite);
	Kinematic& Kin();
	sf::Sprite * Sprite();
	bool Iter(float dt);
	float Angle(void);
	void Damage(float damage);
	bool IsDead(void);
	virtual ~Static();
private:
	Kinematic kin;
	sf::Sprite sprite;
};

#endif /* STATIC_HPP_ */
