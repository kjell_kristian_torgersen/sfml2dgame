/*
 * Player.hpp
 *
 *  Created on: Apr 23, 2017
 *      Author: kjell
 */

#ifndef PLAYER_HPP_
#define PLAYER_HPP_

#include "IEntity.hpp"

class Player: public IEntity {
public:
	Player(void);
	Player(sf::Sprite sprite, sf::Vector2f pos, float health);
	Kinematic& Kin();
	sf::Sprite * Sprite();
	bool Iter(float dt);
	float Angle(void);
	void Damage(float damage);
	bool IsDead(void);
	~Player();
private:
	sf::Sprite sprite;
	Kinematic kin;
	float health;
	bool dead;
};

#endif /* PLAYER_HPP_ */
