/*
 * Game.hpp
 *
 *  Created on: Apr 18, 2017
 *      Author: kjell
 */

#ifndef GAME_HPP_
#define GAME_HPP_

#include "global.hpp"
#include <memory>
#include <vector>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "Projectile.hpp"
#include "Player.hpp"
#include "IEntity.hpp"

class Player;

class Game {
public:
	Game(void);
	void Run(void);
	sf::Vector2i LevelSize();
	void Draw(void);
	void HandleEvents(void);
	sf::Sprite GetSprite(int x, int y);
	sf::Sprite GetSprite(int idx);
	virtual ~Game();
	std::vector<std::unique_ptr<IEntity>>& Monsters(void);
	std::vector<std::unique_ptr<IEntity>>& Entities(void);
	Player * GetPlayer();
	float RandF(void);
	unsigned long RandI(void);
private:
	void Iterate(float dt);
	void KeyPressed(sf::Event& event);
	void KeyReleased(sf::Event& event);
	std::mt19937 rng;
	std::uniform_real_distribution<float> rnd;
	sf::Vector2i levelSize;
	sf::RenderWindow window;
	sf::Image image;
	sf::Texture texture;
	std::vector<sf::Sprite> sprites;
	std::vector<std::unique_ptr<IEntity>> monsters;
	std::vector<std::unique_ptr<IEntity>> entities;
	Player player;
	sf::View view;
	sf::SoundBuffer buffer;
	sf::Sound sound;
};

#endif /* GAME_HPP_ */
