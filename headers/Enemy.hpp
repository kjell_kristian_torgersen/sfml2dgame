/*
 * Enemy.hpp
 *
 *  Created on: Apr 19, 2017
 *      Author: kjell
 */

#ifndef ENEMY_HPP_
#define ENEMY_HPP_

#include <memory>
#include "IEntity.hpp"
#include "IWeapon.hpp"
#include "ITactic.hpp"
#include "Game.hpp"
#include <stack>

class Enemy: public IEntity {
public:
	Enemy(sf::Vector2f pos, sf::Sprite sprite, std::unique_ptr<IWeapon> weapon);
	Kinematic& Kin(void);
	sf::Sprite * Sprite(void);
	bool Iter(float dt);
	float Angle(void);
	void Damage(float damage);
	std::unique_ptr<IWeapon>& Weapon(void);
	void Weapon(std::unique_ptr<IWeapon> weapon);
	void SetTactic(std::unique_ptr<ITactic> tactic);
	bool IsDead(void);
	virtual ~Enemy();
private:
	std::unique_ptr<IWeapon> weapon;
	std::stack<std::unique_ptr<ITactic>> tactic;
	Kinematic kin;
	sf::Sprite sprite;
	float health;
	bool dead;
};

#endif /* ENEMY_HPP_ */
