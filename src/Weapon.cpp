/*
 * Weapon.cpp
 *
 *  Created on: Apr 20, 2017
 *      Author: kjell
 */

#include "Weapon.hpp"
#include "global.hpp"
#include "Game.hpp"
#include "Player.hpp"
#include "Projectile.hpp"

bool Weapon::Fire(sf::Vector2f target)
{
	auto pos = owner->Kin().Pos;
	Projectile * proj = new Projectile(pos, sf::Vector2f(), game->GetSprite(57, 9), owner, 1.0);
	float angle = atan2f(target.y - proj->Kin().Pos.y, target.x - proj->Kin().Pos.x);
	float s = 7 * 32;
	proj->Kin().Vel = {s * cosf(angle), s * sinf(angle)};
	game->Entities().emplace_back(proj);
	return true;
}

bool Weapon::Fire(IEntity* target)
{
	//Player * player = game->GetPlayer();
	auto pos = owner->Kin().Pos;
	Projectile * proj = new Projectile(pos, sf::Vector2f(), game->GetSprite(57, 9), owner, 1.0);
	float s = 7 * 32;
	float t = time_of_impact(target->Kin().Pos - proj->Kin().Pos, target->Kin().Vel, s);
	sf::Vector2f targete = target->Kin().Pos + t * target->Kin().Vel;
	float angle = atan2f(targete.y - proj->Kin().Pos.y, targete.x - proj->Kin().Pos.x);
	proj->Kin().Vel = {s * cosf(angle), s * sinf(angle)};
	//std::cout << "t=" << t << " angle=" << angle << std::endl;
	game->Entities().emplace_back(proj);
	return true;
}

Weapon::Weapon() :
		owner(nullptr)
{

}

Weapon::~Weapon()
{
	// TODO Auto-generated destructor stub
}

void Weapon::SetOwner(IEntity* owner)
{
	this->owner = owner;
}
