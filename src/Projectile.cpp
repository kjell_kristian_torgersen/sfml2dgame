/*
 * Projectile.cpp
 *
 *  Created on: Apr 18, 2017
 *      Author: kjell
 */

#include "Projectile.hpp"
#include "Game.hpp"
#include <cmath>

bool Projectile::Iter(float dt)
{
	if (damage > 0) {
		kin.Iter(dt);
		sprite.setRotation(180.0f * atan2f(kin.Vel.y, kin.Vel.x) / (float) M_PI);
		sprite.setPosition(kin.Pos);
		for (auto& ent : game->Monsters()) {
			if ((ent.get() != attacker) && (!ent->IsDead())) {
				auto dist = ent->Kin().Pos - kin.Pos;
				if (sqrtf(dist.x * dist.x + dist.y * dist.y) < 32.0) {
					ent->Damage(damage);
					this->damage = 0;
				}
			}
		}

		if (attacker != game->GetPlayer()) {
			auto dist = game->GetPlayer()->Kin().Pos - kin.Pos;
			if (sqrtf(dist.x * dist.x + dist.y * dist.y) < 12.0) {
				game->GetPlayer()->Damage(damage);
				this->damage = 0;
			}
		}
		return false;
	} else {
		return true;
	}
}

Projectile::Projectile(sf::Vector2f pos, sf::Vector2f vel, sf::Sprite sprite, IEntity * attacker, float damage) :
		kin(pos, vel), sprite(sprite), attacker(attacker), damage(damage)
{

}

void Projectile::Damage(float damage)
{
	this->damage -= damage;
	if (this->damage < 0) {
		this->damage = 0;
	}
}

/*float Projectile::Angle(void)
 {
 return atan2(kin.Vel.y, kin.Vel.x);
 }

 void Projectile::Angle(float angle)
 {
 float speed = sqrtf(kin.Vel.x * kin.Vel.x + kin.Vel.y * kin.Vel.y);
 kin.Vel.x = speed * cos(angle);
 kin.Vel.y = speed * sin(angle);
 }

 int Projectile::Sprite(void)
 {
 return kin.Sprite;
 }

 void Projectile::Sprite(int sprite)
 {
 kin.Sprite = sprite;
 }*/

Projectile::~Projectile()
{
	// TODO Auto-generated destructor stub
}

Kinematic& Projectile::Kin()
{
	return kin;
}

float Projectile::Angle(void)
{
	return atan2f(kin.Vel.y, kin.Vel.x);
}

sf::Sprite* Projectile::Sprite()
{
	if (damage > 0) {
		return &sprite;
	} else {
		return nullptr;
	}
}

bool Projectile::IsDead(void)
{
	return true;
}
