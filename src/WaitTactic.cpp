/*
 * WaitTactic.cpp
 *
 *  Created on: Apr 19, 2017
 *      Author: kjell
 */

#include "WaitTactic.hpp"
#include "AttackTactic.hpp"

WaitTactic::WaitTactic(Enemy* enemy)
{
	this->enemy = enemy;
}

bool WaitTactic::Iter(float dt)
{
	auto dist = game->GetPlayer()->Kin().Pos - enemy->Kin().Pos;
	if (sqrtf(dist.x * dist.x + dist.y * dist.y) < 320) {
		enemy->SetTactic(std::make_unique<AttackTactic>(enemy));
	}
	return true;
}

WaitTactic::~WaitTactic()
{
	// TODO Auto-generated destructor stub
}

