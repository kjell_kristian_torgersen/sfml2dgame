/*
 * Homing.cpp
 *
 *  Created on: Apr 24, 2017
 *      Author: kjell
 */

#include "Homing.hpp"
#include "Game.hpp"

Homing::Homing(sf::Vector2f pos, sf::Sprite sprite) :
		kin(pos, { }), sprite(sprite), done(false)
{
}

Kinematic& Homing::Kin()
{
	return kin;
}

sf::Sprite* Homing::Sprite()
{
	return &sprite;
}

bool Homing::Iter(float dt)
{
	if (!done) {
		auto pos = kin.Pos;

		int minidx = 0;
		float minval = 1e99;
		int i = 0;
		for (auto& mon : game->Monsters()) {
			if (!mon->IsDead()) {
				float d = dist(mon->Kin().Pos - pos);
				if (d < 16.0) {
					mon->Damage(100);
					done = true;
					kin.Vel = sf::Vector2f();
					break;
				}
				if (d < minval) {
					minval = d;
					minidx = i;
				}
			}
			i++;
		}
		auto ang = game->Monsters()[minidx]->Kin().Pos - pos;

		float angle = atan2f(ang.y, ang.x);
		float speed = 5*32;
		kin.Vel = sf::Vector2f(speed * cosf(angle), speed * sinf(angle));
		kin.Iter(dt);
		sprite.setPosition(kin.Pos);
	}

}

float Homing::Angle(void)
{
	return 0;
}

void Homing::Damage(float damage)
{

}

bool Homing::IsDead(void)
{
	return true;
}

Homing::~Homing()
{

}

