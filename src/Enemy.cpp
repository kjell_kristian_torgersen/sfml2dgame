/*
 * Enemy.cpp
 *
 *  Created on: Apr 19, 2017
 *      Author: kjell
 */

#include "Enemy.hpp"
#include "WaitTactic.hpp"
#include "Static.hpp"
#include <memory>

Enemy::Enemy(sf::Vector2f pos, sf::Sprite sprite, std::unique_ptr<IWeapon> weapon) :
		kin(pos, { 0, 0 }), sprite(sprite), health(100.0), dead(false)
{
	tactic.push(std::make_unique<WaitTactic>(this));
	this->weapon = std::move(weapon);
	this->weapon->SetOwner(this);
}

float Enemy::Angle(void)
{
	return 0;
}

bool Enemy::Iter(float dt)
{
	if (health > 0) {
		kin.Iter(dt);
		sprite.setPosition(kin.Pos);
		if (!(tactic.top()->Iter(dt))) {
			//delete tactic.top();
			tactic.pop();
		}
		return true;
	} else {
		if (!dead) {
			dead = true;
			sprite.rotate(90);
			game->Entities().emplace_back(new Static(kin.Pos + sf::Vector2f(0.0, 8.0), game->GetSprite(3 + (int)(game->RandI() % 5), 0)));
		}
		return true;
	}
}

std::unique_ptr<IWeapon>& Enemy::Weapon()
{
	return weapon;
}

void Enemy::Weapon(std::unique_ptr<IWeapon> weapon)
{
	this->weapon = std::move(weapon);
}

void Enemy::SetTactic(std::unique_ptr<ITactic> tactic)
{
	this->tactic.push(std::move(tactic));
}

Kinematic& Enemy::Kin(void)
{
	return kin;
}

sf::Sprite * Enemy::Sprite(void)
{
	return &sprite;
}

bool Enemy::IsDead(void)
{
	return dead;
}

Enemy::~Enemy()
{
}

void Enemy::Damage(float damage)
{
	this->health -= damage;
}
