/*
 * Player.cpp
 *
 *  Created on: Apr 23, 2017
 *      Author: kjell
 */
#include <iostream>
#include "Player.hpp"
#include "Game.hpp"
#include "Static.hpp"

Kinematic& Player::Kin()
{
	return kin;
}

sf::Sprite* Player::Sprite()
{
	return &sprite;
}

bool Player::Iter(float dt)
{
	if (health > 0) {
		kin.Iter(dt);
		sprite.setPosition(kin.Pos);
		return true;
	} else {
		sprite.setPosition(kin.Pos);
		return false;
	}
}

float Player::Angle(void)
{
	return atan2f(kin.Vel.y, kin.Vel.x);
}

void Player::Damage(float damage)
{
	this->health -= damage;
	if ((health <= 0) && (!dead)) {
		dead = true;
		sprite.rotate(90);
		game->Entities().emplace_back(new Static(kin.Pos + sf::Vector2f(0.0, 8.0), game->GetSprite(5, 0)));
	}
	std::cout << "health=" << health << std::endl;
}

Player::Player(sf::Sprite sprite, sf::Vector2f pos, float health) :
		sprite(sprite), kin(pos, { 0, 0 }), health(health), dead(false)
{
}


Player::~Player()
{
}

bool Player::IsDead(void)
{
	return dead;
}
