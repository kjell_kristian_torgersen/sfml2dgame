/*
 * Static.cpp
 *
 *  Created on: Apr 24, 2017
 *      Author: kjell
 */

#include "Static.hpp"



Kinematic& Static::Kin()
{
	return kin;
}

sf::Sprite* Static::Sprite()
{
	return &sprite;
}

bool Static::Iter(float dt)
{
	kin.Iter(dt);
	sprite.setPosition(kin.Pos);
	return false;
}

float Static::Angle(void)
{
	return 0;
}

void Static::Damage(float damage)
{
}

Static::Static(sf::Vector2f pos, sf::Sprite sprite) : kin(pos,{0,0}), sprite(sprite)
{
	this->sprite.setPosition(pos);
}

Static::~Static()
{
	// TODO Auto-generated destructor stub
}

bool Static::IsDead(void)
{
	return true;
}


