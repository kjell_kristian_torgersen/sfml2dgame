/*
 * Kinematic.cpp
 *
 *  Created on: Apr 20, 2017
 *      Author: kjell
 */

#include "Kinematic.hpp"

Kinematic::Kinematic(sf::Vector2f pos, sf::Vector2f vel) : Pos(pos), Vel(vel)
{
}

void Kinematic::Iter(float dt)
{
	Pos = Pos + Vel*dt;
}

Kinematic::~Kinematic()
{
	// TODO Auto-generated destructor stub
}

Kinematic::Kinematic(void) : Pos({0,0}), Vel({0,0})
{
}

