/*
 * Game.cpp
 *
 *  Created on: Apr 18, 2017
 *      Author: kjell
 */

#include "Game.hpp"
#include <cmath>
#include <iostream>
#include "Projectile.hpp"
#include "Enemy.hpp"
#include "Weapon.hpp"
#include "IWeapon.hpp"
#include <SFML/Audio.hpp>
#include "Homing.hpp"

Game::Game() :
		rng(0), rnd(0.0, 1.0), levelSize(0, 0), window(sf::VideoMode(800, 600), "SFML window"), player(sf::Sprite(), { }, 100), view(sf::FloatRect(0, 0, 800, 600))
{


	buffer.loadFromFile("/home/kjell/virtual/shot1.wav");
	texture.loadFromFile("/home/kjell/virtual/sprites.png");
	/*for (uint y = 0; y < texture.getSize().y / 32; y++) {
	 for (uint x = 0; x < texture.getSize().x / 32; x++) {
	 sf::Sprite sprite(texture, { (int) (32 * x), (int) (32 * y), 32, 32 });
	 sprite.setOrigin(16, 16);
	 sprites.push_back(sprite);
	 }
	 }*/
	player = Player(GetSprite(32, 1), { 0, 0 }, 100.0);
	//Enemy * e = new Enemy();
	for (int i = 0; i < 50; i++) {
		monsters.push_back(
				std::make_unique<Enemy>(sf::Vector2f(400.0f + 64.0f * (float) (i % 10), 300.0f + 64.0f * (float) (i / 10)), GetSprite(64 + 9 + i),
						std::make_unique<Weapon>()));
	}
}

void Game::Run(void)
{
	sf::Clock clock; // starts the clock
	window.setFramerateLimit(60);
	while (window.isOpen()) {
		HandleEvents();
		Iterate(clock.restart().asSeconds());
		Draw();
	}
}

sf::Vector2i Game::LevelSize()
{
	sf::Vector2i ret;
	return ret;
}

void Game::Draw(void)
{
	view.setCenter(player.Kin().Pos);
	window.setView(view);
	window.clear(sf::Color(0, 128, 0, 255));
	for (auto& ent : entities) {
		sf::Sprite * s = ent->Sprite();
		if (s) {
			window.draw(*s);
		}
	}
	for (auto& mon : monsters) {
		window.draw(*mon->Sprite());
	}
	window.draw(*player.Sprite());

	window.display();
}

float time_of_impact(sf::Vector2f p, sf::Vector2f v, float s)
{
	float a = s * s - (v.x * v.x + v.y * v.y);
	float b = p.x * v.x + p.y * v.y;
	float c = p.x * p.x + p.y * p.y;

	float d = b * b + a * c;

	float t = 0;
	if (d >= 0) {
		t = (b + sqrtf(d)) / a;
		if (t < 0)
			t = 0;
	}

	return t;
}

void Game::HandleEvents(void)
{
	// Process events
	sf::Event event;
	while (window.pollEvent(event)) {
		// Close window : exit
		switch (event.type) {
		case sf::Event::Closed:
			window.close();
			break;
		case sf::Event::KeyPressed:
			KeyPressed(event);
			break;
		case sf::Event::KeyReleased:
			KeyReleased(event);
			break;
		case sf::Event::Resized:
			view = sf::View(sf::FloatRect(0, 0, (float) event.size.width, (float) event.size.height));
			view.setCenter(player.Kin().Pos);
			break;
		default:
			break;
		}
	}

}

Game::~Game()
{

}

sf::Sprite Game::GetSprite(int x, int y)
{
	sf::Sprite ret(texture, { 32 * x, 32 * y, 32, 32 });
	ret.setOrigin(16, 16);
	return ret;
}

sf::Sprite Game::GetSprite(int idx)
{
	int w = texture.getSize().x / 32;
	int x = idx % w;
	int y = idx / w;
	sf::Sprite ret(texture, { 32 * x, 32 * y, 32, 32 });
	ret.setOrigin(16, 16);
	return ret;
}

std::vector<std::unique_ptr<IEntity>>& Game::Monsters(void)
{
	return monsters;
}

std::vector<std::unique_ptr<IEntity>>& Game::Entities(void)
{
	return entities;
}

Player * Game::GetPlayer()
{
	return &player;
}

void Game::Iterate(float dt)
{
	//for (auto& mon : monsters)
	for (uint i = 0; i < monsters.size(); i++) {
		monsters[i]->Iter(dt);
	}

	player.Iter(dt);
	for (uint i = 0; i < entities.size(); i++) {
		entities[i]->Iter(dt);
	}

}

float dist(sf::Vector2f v)
{
	return sqrtf(v.x * v.x + v.y * v.y);
}

void Game::KeyPressed(sf::Event& event)
{
	switch (event.key.code) {
	case sf::Keyboard::Right:
		player.Kin().Vel.x = 3 * 32;
		break;
	case sf::Keyboard::Left:
		player.Kin().Vel.x = -3 * 32;
		break;
	case sf::Keyboard::Up:
		player.Kin().Vel.y = -3 * 32;
		break;
	case sf::Keyboard::Down:
		player.Kin().Vel.y = 3 * 32;
		break;
	case sf::Keyboard::A:
		view.zoom(2);
		break;
	case sf::Keyboard::Z:
		view.zoom(0.5);
		break;
	case sf::Keyboard::Space: {
		/*auto pos = player.Kin().Pos;
		float distmin = 1e99;
		int distidx = 0;
		int i = 0;
		for (auto& mon : monsters) {
			if (!mon->IsDead()) {
				float d = dist(pos - mon->Kin().Pos);
				if (d < distmin) {
					distmin = d;
					distidx = i;
				}
			}
			i++;
		}

		Projectile * proj = new Projectile(pos, sf::Vector2f(), GetSprite(RandI() % 7, 10), &player, 100.0f);
		float s = 7 * 32;
		float t = time_of_impact(monsters[distidx]->Kin().Pos - proj->Kin().Pos, monsters[distidx]->Kin().Pos, s);
		sf::Vector2f targete = monsters[distidx]->Kin().Pos + t * monsters[distidx]->Kin().Vel;
		float angle = atan2f(targete.y - proj->Kin().Pos.y, targete.x - proj->Kin().Pos.x);
		proj->Kin().Vel = {s * cosf(angle), s * sinf(angle)};
		game->Entities().emplace_back(proj);*/
		entities.emplace_back(new Homing(player.Kin().Pos, GetSprite(RandI() % 7, 10)));
		sound.setBuffer(buffer);
		sound.play();
	}
		break;
	default:
		break;
	}
}

float Game::RandF(void)
{
	return rnd(rng);
}

unsigned long Game::RandI(void)
{
	return rng();
}

void Game::KeyReleased(sf::Event& event)
{
	switch (event.key.code) {
	case sf::Keyboard::Right:
		player.Kin().Vel.x = 0;
		break;
	case sf::Keyboard::Left:
		player.Kin().Vel.x = 0;
		break;
	case sf::Keyboard::Up:
		player.Kin().Vel.y = 0;
		break;
	case sf::Keyboard::Down:
		player.Kin().Vel.y = 0;
		break;
	default:
		break;
	}

}
