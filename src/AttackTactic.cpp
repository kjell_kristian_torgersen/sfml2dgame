/*
 * AttackTactic.cpp
 *
 *  Created on: Apr 21, 2017
 *      Author: kjell
 */

#include "AttackTactic.hpp"

AttackTactic::AttackTactic(Enemy* enemy) :
		enemy(enemy), timer(5.0f * game->RandF()), speed(64.0f * (game->RandF() - 0.5f)), range(320.0f * (game->RandF() + 0.5f))
{
}

bool AttackTactic::Iter(float dt)
{
	if (!game->GetPlayer()->IsDead()) {
		auto dist = game->GetPlayer()->Kin().Pos - enemy->Kin().Pos;
		timer += dt;
		if (sqrtf(dist.x * dist.x + dist.y * dist.y) > 640) {
			return false;
		} else {
			if (timer >= 3.0) {
				enemy->Weapon()->Fire(game->GetPlayer());
				timer = 0.0;
			} else {
				float angle = atan2f(dist.y, dist.x);
				if (sqrtf(dist.x * dist.x + dist.y * dist.y) > range) {
					enemy->Kin().Vel = {speed*cosf(angle), speed*sinf(angle)};
				} else {
					enemy->Kin().Vel = {speed*cosf(angle+(float)M_PI/2.0f), speed*sinf(angle+(float)M_PI/2.0f)};
				}
			}
			return true;
		}
	} else {
		return true;
	}
}

AttackTactic::~AttackTactic()
{
	// TODO Auto-generated destructor stub
}

