/*
 * IWeapon.hpp
 *
 *  Created on: Apr 19, 2017
 *      Author: kjell
 */

#ifndef IWEAPON_HPP_
#define IWEAPON_HPP_

#include "global.hpp"
#include "IEntity.hpp"

class IWeapon {
public:
	virtual bool Fire(sf::Vector2f target)=0;
	virtual bool Fire(IEntity * target)=0;
	virtual void SetOwner(IEntity * owner)=0;
	virtual ~IWeapon(){}
};

#endif /* IWEAPON_HPP_ */
