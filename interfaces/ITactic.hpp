/*
 * ITactic.hpp
 *
 *  Created on: Apr 19, 2017
 *      Author: kjell
 */

#ifndef ITACTIC_HPP_
#define ITACTIC_HPP_

class ITactic {
public:
	virtual bool Iter(float dt)=0;
	virtual ~ITactic(){}
};

#endif /* ITACTIC_HPP_ */
