/*
 * IEntity.hpp
 *
 *  Created on: Apr 18, 2017
 *      Author: kjell
 */

#ifndef IENTITY_HPP_
#define IENTITY_HPP_
#include "global.hpp"
#include "Kinematic.hpp"
#include <SFML/Graphics.hpp>
class Game;

class IEntity {
public:
	virtual Kinematic& Kin()=0;
	virtual sf::Sprite * Sprite()=0;
	virtual bool Iter(float dt)=0;
	virtual float Angle(void)=0;
	virtual void Damage(float damage)=0;
	virtual bool IsDead(void)=0;
	virtual ~IEntity()
	{
	}
};

#endif /* IENTITY_HPP_ */
